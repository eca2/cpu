module CPU_Fixed where
import Clash.Prelude

type Clk = Clock System
type Rst = Reset System
type Sig = Signal System

type Val = Unsigned 16
type Vector = Vec 32 Val


-- Student information:
--  Student 1
--    lastname: Battistig
--    student number: s1716239
--  Student 2
--    lastname: Pennestri
--    student number: s2382660

data Opc = Add | Mul
 deriving (Show, Generic, NFDataX, Eq)

data Value = Cst Val | Addr Val | Top
 deriving (Show, Generic, NFDataX, Eq)

data Instr = Push Value | Calc Opc | Send Value | Pop | Idle | ResetPc
 deriving (Show, Generic, NFDataX, Eq)

type ProgMemT = Vec 32 Instr


-- We have considered a 32 instruction memory, because 32 is the nearest power of 2 from 24.
initProgMem :: ProgMemT
initProgMem = (Push ( Cst 2 ) :> Push ( Addr 0 ) :> Pop :> Calc Mul :> Push ( Cst 3 ) :> Push ( Cst 4 ) :> Push ( Addr 1 ) :> Pop :> Calc Add :> Pop :> Calc Mul :> Pop :> Calc Add :> Push ( Cst 12 ) :> Push ( Cst 5 ) :> Pop :> Calc Add :> Pop :> Calc Mul :> Send Top :> Push ( Cst 2 ) :> Send Top :> Pop :> Send Top:> Idle:>Idle:>Idle:>Idle:>Idle:>Idle:>Idle:>ResetPc:> Nil)

-- initProgMem1 contains the original program provided by the assignment
initProgMem1:: Vec 24 Instr
initProgMem1 = (Push ( Cst 2 ) :> Push ( Addr 0 ) :> Pop :> Calc Mul :> Push ( Cst 3 ) :> Push ( Cst 4 ) :> Push ( Addr 1 ) :> Pop :> Calc Add :> Pop :> Calc Mul :> Pop :> Calc Add :> Push ( Cst 12 ) :> Push ( Cst 5 ) :> Pop :> Calc Add :> Pop :> Calc Mul :> Send Top :> Push ( Cst 2 ) :> Send Top :> Pop :> Send Top :> Nil)

-- initstack contatins the initial stack memory 
initstack::Vector
initstack = replicate d32 0

-- initiheap contaons the initial heap
initheap:: Vector
initheap = (10:>Nil) ++ ( (11:>Nil) ++ (replicate d30 0) )

alu :: Opc -> Val -> Val -> Val
alu Add x y = x + y
alu Mul x y = x * y

execute :: (Vector , Vector , Val) -> Instr -> ((Vector , Vector, Val), Maybe(Val))
execute (stk , heap, register) (Calc Add) = ((stk' , heap, register),Nothing)
     where
        operator1 = head stk
        stk' = (alu Add operator1 register) +>> (stk <<+ 0)


execute (stk , heap, register) (Calc Mul) = ((stk' , heap, register),Nothing)
     where
        operator1 = head stk
        stk' = (alu Mul operator1 register) +>> (stk <<+ 0)


execute (stk , heap, register) (Pop) = (( ( (tail stk) ++ (0:>Nil) ) , heap, (head stk)),Nothing)


execute (stk, heap, register) (Push (Cst x)) = ((stk' , heap, register), Nothing)
     where
        stk' = x +>> stk


execute (stk, heap, register) (Push (Addr x)) = ((stk' , heap, register), Nothing)
     where
        stk' = (heap !! x) +>> stk


execute (stk , heap, register) (Send (Cst x)) = ((stk , heap, register), Just (x) )
execute (stk , heap, register) (Send (Addr x)) = ((stk , heap, register), Just (heap !! x))
execute (stk , heap, register) (Send Top) = ((stk , heap, register), Just (stk !! 0))
execute (stk, heap, register) (Idle) = ((stk , heap, register), Nothing)


core ::  (ProgMemT, Unsigned 5 ,Vector , Vector , Val) -> (Bool, Instr)-> ((ProgMemT, Unsigned 5 ,Vector, Vector, Val), Maybe(Val) )
core (programMem, pc ,stk, heap, register) (is_write, instr) = ((programMem', pc' ,stk', heap', register'), out )
       where
        current_instr = programMem !! pc
        is_idle = current_instr == Idle
        is_reset = current_instr == ResetPc
        pc' 
          |(is_idle || is_write) = pc
          |is_reset = 0
          |otherwise = pc + 1

        programMem'
              |is_write = instr +>> programMem
              |otherwise = programMem
        ((stk' , heap', register'), out) = execute (stk, heap, register) (current_instr)


coreSim :: HiddenClockResetEnable dom  => Signal dom ((Bool, Instr)) -> Signal dom (Maybe(Val))
coreSim = mealy core (initProgMem, 0, initstack, initheap, 0)
testList = toList $ replicate d32 ((False, Idle))
-- simulate @System coreSim testList


topEntity :: Clk -> Rst -> Sig (Bool, Instr) -> Sig (Maybe(Val))
topEntity clk rst x = withClockResetEnable clk rst enableGen (mealy core (initProgMem, 0, initstack, initheap, 0) ) x
{-# NOINLINE alu #-}
{-# NOINLINE execute #-}


-- Alternative implementation of core function:
core2 :: Vec 24 Instr ->  (Unsigned 5 ,Vector , Vector , Val) -> (Bool)-> ((Unsigned 5 ,Vector, Vector, Val), Maybe(Val) )
core2 programMem (pc ,stk, heap, register) (is_execute) = ((pc' ,stk', heap', register'), out )
       where
        current_instr = programMem !! pc
        is_out_of_bound = pc == 23
        pc' 
          |is_out_of_bound = 0
          |is_execute = pc + 1
          |otherwise = pc
        --pc' = pc + 1
        ((stk' , heap', register'), out) = execute (stk, heap, register) (current_instr)


coreSim2 :: HiddenClockResetEnable dom  => Signal dom (Bool) -> Signal dom (Maybe(Val))
coreSim2 = mealy (core2 initProgMem1) (0, initstack, initheap, 0)
-- simulate @System coreSim2 testList2
testList2 = toList $ replicate d48 (True)

{--
topEntity :: Clk -> Rst -> Sig (Bool) -> Sig (Maybe(Val))
topEntity clk rst x = withClockResetEnable clk rst enableGen ( mealy (core2 initProgMem1) (0, initstack, initheap, 0) ) x
{-# NOINLINE alu #-}
{-# NOINLINE execute #-}
--}