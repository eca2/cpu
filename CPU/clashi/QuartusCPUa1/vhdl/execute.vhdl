-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.cpu_fixed_types.all;

entity execute is
  port(ds           : in cpu_fixed_types.tup3;
       ds1          : in cpu_fixed_types.instr;
       \c$case_alt\ : out cpu_fixed_types.tup2);
end;

architecture structural of execute is
  signal \c$case_alt_0\           : cpu_fixed_types.tup2;
  signal \c$case_alt_1\           : cpu_fixed_types.tup2;
  signal result                   : cpu_fixed_types.tup2;
  -- CPU_Fixed.hs:53:1-7
  signal ds2                      : cpu_fixed_types.value;
  -- CPU_Fixed.hs:53:1-7
  signal ds2_0                    : cpu_fixed_types.opc;
  -- CPU_Fixed.hs:53:1-7
  signal ds2_1                    : cpu_fixed_types.value;
  -- CPU_Fixed.hs:53:1-7
  signal heap                     : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  signal \c$app_arg\              : unsigned(15 downto 0);
  -- CPU_Fixed.hs:53:1-7
  signal stk                      : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:53:1-7
  signal \register\               : unsigned(15 downto 0);
  -- CPU_Fixed.hs:53:1-7
  signal x                        : unsigned(15 downto 0);
  signal \c$app_arg_0\            : unsigned(15 downto 0);
  signal result_0                 : unsigned(15 downto 0);
  -- CPU_Fixed.hs:53:1-7
  signal wild3                    : signed(63 downto 0);
  -- CPU_Fixed.hs:53:1-7
  signal x_0                      : unsigned(15 downto 0);
  signal \c$aluOut\               : unsigned(15 downto 0);
  -- CPU_Fixed.hs:53:1-7
  signal x_1                      : unsigned(15 downto 0);
  signal result_1                 : unsigned(15 downto 0);
  signal \c$aluOut_app_arg\       : cpu_fixed_types.opc;
  -- CPU_Fixed.hs:53:1-7
  signal wild3_0                  : signed(63 downto 0);
  -- CPU_Fixed.hs:53:1-7
  signal x_2                      : unsigned(15 downto 0);
  signal \c$vec\                  : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_1_sel_alt\   : cpu_fixed_types.tup2_0;
  signal \c$vec_0\                : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_1_sel_alt_0\ : cpu_fixed_types.tup2_0;
  signal \c$vec_1\                : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal result_sel_alt           : cpu_fixed_types.tup2_1;
  signal \c$vec_2\                : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal result_sel_alt_0         : cpu_fixed_types.tup2_0;
  signal \c$vec_3\                : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal result_sel_alt_1         : cpu_fixed_types.tup2_1;
  signal \c$vec_4\                : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal result_sel_alt_2         : cpu_fixed_types.tup2_0;

begin
  with (ds1(20 downto 18)) select
    \c$case_alt\ <= \c$case_alt_1\ when "000",
                    result when "001",
                    \c$case_alt_0\ when "010",
                    ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'((stk(1 to stk'high))) & cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => to_unsigned(0,16))))
                    , tup3_sel1_array_of_unsigned_16_1 => heap
                    , tup3_sel2_unsigned => \c$app_arg\ )
                    , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when "011",
                    ( tup2_sel0_tup3 => ds
                    , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when "100",
                    cpu_fixed_types.tup2'( cpu_fixed_types.tup3'( cpu_fixed_types.array_of_unsigned_16'(0 to 31 => unsigned'(0 to 15 => '-')), cpu_fixed_types.array_of_unsigned_16'(0 to 31 => unsigned'(0 to 15 => '-')), unsigned'(0 to 15 => '-') ), cpu_fixed_types.maybe'(0 to 16 => '-') ) when others;

  with (ds2(17 downto 16)) select
    \c$case_alt_0\ <= ( tup2_sel0_tup3 => ds
                      , tup2_sel1_maybe => std_logic_vector'("1" & (std_logic_vector(x))) ) when "00",
                      ( tup2_sel0_tup3 => ds
                      , tup2_sel1_maybe => std_logic_vector'("1" & (std_logic_vector(result_0))) ) when "01",
                      ( tup2_sel0_tup3 => ds
                      , tup2_sel1_maybe => std_logic_vector'("1" & (std_logic_vector(\c$app_arg_0\))) ) when others;

  \c$vec\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => x_1)) & cpu_fixed_types.array_of_unsigned_16'(stk)));

  \c$case_alt_1_sel_alt\ <= (\c$vec\(0 to 32-1),\c$vec\(32 to \c$vec\'high));

  \c$vec_0\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => result_1)) & cpu_fixed_types.array_of_unsigned_16'(stk)));

  \c$case_alt_1_sel_alt_0\ <= (\c$vec_0\(0 to 32-1),\c$vec_0\(32 to \c$vec_0\'high));

  with (ds2_1(17 downto 16)) select
    \c$case_alt_1\ <= ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => \c$case_alt_1_sel_alt\.tup2_0_sel0_array_of_unsigned_16_0
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when "00",
                      ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => \c$case_alt_1_sel_alt_0\.tup2_0_sel0_array_of_unsigned_16_0
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when "01",
                      cpu_fixed_types.tup2'( cpu_fixed_types.tup3'( cpu_fixed_types.array_of_unsigned_16'(0 to 31 => unsigned'(0 to 15 => '-')), cpu_fixed_types.array_of_unsigned_16'(0 to 31 => unsigned'(0 to 15 => '-')), unsigned'(0 to 15 => '-') ), cpu_fixed_types.maybe'(0 to 16 => '-') ) when others;

  \c$vec_1\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(stk) & cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => to_unsigned(0,16)))));

  result_sel_alt <= (\c$vec_1\(0 to 1-1),\c$vec_1\(1 to \c$vec_1\'high));

  \c$vec_2\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => \c$aluOut\)) & cpu_fixed_types.array_of_unsigned_16'(result_sel_alt.tup2_1_sel1_array_of_unsigned_16_1)));

  result_sel_alt_0 <= (\c$vec_2\(0 to 32-1),\c$vec_2\(32 to \c$vec_2\'high));

  \c$vec_3\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(stk) & cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => to_unsigned(0,16)))));

  result_sel_alt_1 <= (\c$vec_3\(0 to 1-1),\c$vec_3\(1 to \c$vec_3\'high));

  \c$vec_4\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => \c$aluOut\)) & cpu_fixed_types.array_of_unsigned_16'(result_sel_alt_1.tup2_1_sel1_array_of_unsigned_16_1)));

  result_sel_alt_2 <= (\c$vec_4\(0 to 32-1),\c$vec_4\(32 to \c$vec_4\'high));

  with (ds2_0) select
    result <= ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => result_sel_alt_0.tup2_0_sel0_array_of_unsigned_16_0
              , tup3_sel1_array_of_unsigned_16_1 => heap
              , tup3_sel2_unsigned => \register\ )
              , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when "0",
              ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => result_sel_alt_2.tup2_0_sel0_array_of_unsigned_16_0
              , tup3_sel1_array_of_unsigned_16_1 => heap
              , tup3_sel2_unsigned => \register\ )
              , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when others;

  ds2 <= ds1(17 downto 0);

  ds2_0 <= ds1(17 downto 17);

  ds2_1 <= ds1(17 downto 0);

  heap <= ds.tup3_sel1_array_of_unsigned_16_1;

  \c$app_arg\ <=  stk(0) ;

  stk <= ds.tup3_sel0_array_of_unsigned_16_0;

  \register\ <= ds.tup3_sel2_unsigned;

  x <= unsigned(ds2(15 downto 0));

  -- index begin
  indexvec : block
    signal vec_index : integer range 0 to 32-1;
  begin
    vec_index <= to_integer(to_signed(0,64))
    -- pragma translate_off
                 mod 32
    -- pragma translate_on
                 ;
    \c$app_arg_0\ <= stk(vec_index);
  end block;
  -- index end

  -- index begin
  indexvec_0 : block
    signal vec_index_0 : integer range 0 to 32-1;
  begin
    vec_index_0 <= to_integer((wild3))
    -- pragma translate_off
                 mod 32
    -- pragma translate_on
                 ;
    result_0 <= heap(vec_index_0);
  end block;
  -- index end

  wild3 <= (signed(std_logic_vector(resize(x_0,64))));

  x_0 <= unsigned(ds2(15 downto 0));

  alu_caluout : entity alu
    port map
      ( result => \c$aluOut\
      , ds     => \c$aluOut_app_arg\
      , x      => \c$app_arg\
      , y      => \register\ );

  x_1 <= unsigned(ds2_1(15 downto 0));

  -- index begin
  indexvec_1 : block
    signal vec_index_1 : integer range 0 to 32-1;
  begin
    vec_index_1 <= to_integer((wild3_0))
    -- pragma translate_off
                 mod 32
    -- pragma translate_on
                 ;
    result_1 <= heap(vec_index_1);
  end block;
  -- index end

  with (ds2_0) select
    \c$aluOut_app_arg\ <= "0" when "0",
                          "1" when others;

  wild3_0 <= (signed(std_logic_vector(resize(x_2,64))));

  x_2 <= unsigned(ds2_1(15 downto 0));


end;

