-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.cpu_fixed_types.all;

entity topentity is
  port(-- clock
       clk   : in cpu_fixed_types.clk_system;
       -- reset
       rst   : in cpu_fixed_types.rst_system;
       x_0   : in boolean;
       x_1   : in cpu_fixed_types.instr;
       \out\ : out cpu_fixed_types.maybe);
end;

architecture structural of topentity is
  signal \c$tup_app_arg\            : unsigned(4 downto 0);
  signal \c$tup_app_arg_0\          : cpu_fixed_types.array_of_instr(0 to 31);
  -- CPU_Fixed.hs:85:1-4
  signal \programMem\               : cpu_fixed_types.array_of_instr(0 to 31);
  -- CPU_Fixed.hs:85:1-4
  signal pc                         : unsigned(4 downto 0);
  -- CPU_Fixed.hs:85:1-4
  signal is_write                   : boolean;
  signal \c$tup_case_alt\           : unsigned(4 downto 0);
  -- CPU_Fixed.hs:85:1-4
  signal ds                         : cpu_fixed_types.tup5;
  signal \c$tup_case_alt_0\         : unsigned(4 downto 0);
  -- CPU_Fixed.hs:85:1-4
  signal \stk'\                     : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:85:1-4
  signal \heap'\                    : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:85:1-4
  signal \register'\                : unsigned(15 downto 0);
  signal result                     : cpu_fixed_types.instr;
  -- CPU_Fixed.hs:85:1-4
  signal ds3                        : cpu_fixed_types.tup3;
  -- CPU_Fixed.hs:85:1-4
  signal \c$ds2_case_scrut\         : cpu_fixed_types.tup2;
  -- CPU_Fixed.hs:85:1-4
  signal wild3                      : signed(63 downto 0);
  -- CPU_Fixed.hs:85:1-4
  signal stk                        : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:85:1-4
  signal heap                       : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:85:1-4
  signal \register\                 : unsigned(15 downto 0);
  -- CPU_Fixed.hs:85:1-4
  signal instr                      : cpu_fixed_types.instr;
  signal x                          : cpu_fixed_types.tup2_2;
  signal \c$vec\                    : cpu_fixed_types.array_of_instr(0 to 32);
  signal \c$tup_app_arg_0_sel_alt\  : cpu_fixed_types.tup2_3;
  signal \c$ds2_case_scrut_fun_arg\ : cpu_fixed_types.tup3;

begin
  x <= ( tup2_2_sel0_boolean => x_0
       , tup2_2_sel1_instr => x_1 );

  with (result(20 downto 18)) select
    \c$tup_app_arg\ <= pc when "100",
                       \c$tup_case_alt\ when others;

  \c$vec\ <= (cpu_fixed_types.array_of_instr'(cpu_fixed_types.array_of_instr'(cpu_fixed_types.array_of_instr'(0 => instr)) & cpu_fixed_types.array_of_instr'(\programMem\)));

  \c$tup_app_arg_0_sel_alt\ <= (\c$vec\(0 to 32-1),\c$vec\(32 to \c$vec\'high));

  \c$tup_app_arg_0\ <= \c$tup_app_arg_0_sel_alt\.tup2_3_sel0_array_of_instr_0 when is_write else
                       \programMem\;

  \programMem\ <= ds.tup5_sel0_array_of_instr;

  pc <= ds.tup5_sel1_unsigned_0;

  is_write <= x.tup2_2_sel0_boolean;

  \c$tup_case_alt\ <= pc when is_write else
                      \c$tup_case_alt_0\;

  -- register begin
  topentity_register : block
    signal ds_reg : cpu_fixed_types.tup5 := ( tup5_sel0_array_of_instr => cpu_fixed_types.array_of_instr'( std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(2,16)))))), std_logic_vector'("000" & (std_logic_vector'("01" & (std_logic_vector(to_unsigned(0,16)))))), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("1") & "-----------------"), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(3,16)))))), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(4,16)))))), std_logic_vector'("000" & (std_logic_vector'("01" & (std_logic_vector(to_unsigned(1,16)))))), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("0") & "-----------------"), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("1") & "-----------------"), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("0") & "-----------------"), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(12,16)))))), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(5,16)))))), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("0") & "-----------------"), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("1") & "-----------------"), std_logic_vector'("010" & (std_logic_vector'("10" & "----------------"))), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(2,16)))))), std_logic_vector'("010" & (std_logic_vector'("10" & "----------------"))), std_logic_vector'("011" & "------------------"), std_logic_vector'("010" & (std_logic_vector'("10" & "----------------"))), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("101" & "------------------") ), tup5_sel1_unsigned_0 => to_unsigned(0,5), tup5_sel2_array_of_unsigned_16_0 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup5_sel3_array_of_unsigned_16_1 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(10,16), to_unsigned(11,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup5_sel4_unsigned_1 => to_unsigned(0,16) );
  begin
    ds <= ds_reg; 
    ds_r : process(clk,rst)
    begin
      if rst =  '1'  then
        ds_reg <= ( tup5_sel0_array_of_instr => cpu_fixed_types.array_of_instr'( std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(2,16)))))), std_logic_vector'("000" & (std_logic_vector'("01" & (std_logic_vector(to_unsigned(0,16)))))), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("1") & "-----------------"), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(3,16)))))), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(4,16)))))), std_logic_vector'("000" & (std_logic_vector'("01" & (std_logic_vector(to_unsigned(1,16)))))), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("0") & "-----------------"), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("1") & "-----------------"), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("0") & "-----------------"), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(12,16)))))), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(5,16)))))), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("0") & "-----------------"), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("1") & "-----------------"), std_logic_vector'("010" & (std_logic_vector'("10" & "----------------"))), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(2,16)))))), std_logic_vector'("010" & (std_logic_vector'("10" & "----------------"))), std_logic_vector'("011" & "------------------"), std_logic_vector'("010" & (std_logic_vector'("10" & "----------------"))), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("101" & "------------------") ), tup5_sel1_unsigned_0 => to_unsigned(0,5), tup5_sel2_array_of_unsigned_16_0 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup5_sel3_array_of_unsigned_16_1 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(10,16), to_unsigned(11,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup5_sel4_unsigned_1 => to_unsigned(0,16) )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      elsif rising_edge(clk) then
        ds_reg <= ( tup5_sel0_array_of_instr => \c$tup_app_arg_0\, tup5_sel1_unsigned_0 => \c$tup_app_arg\, tup5_sel2_array_of_unsigned_16_0 => \stk'\, tup5_sel3_array_of_unsigned_16_1 => \heap'\, tup5_sel4_unsigned_1 => \register'\ )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      end if;
    end process;
  end block;
  -- register end

  with (result(20 downto 18)) select
    \c$tup_case_alt_0\ <= to_unsigned(0,5) when "101",
                          pc + to_unsigned(1,5) when others;

  \out\ <= \c$ds2_case_scrut\.tup2_sel1_maybe;

  \stk'\ <= ds3.tup3_sel0_array_of_unsigned_16_0;

  \heap'\ <= ds3.tup3_sel1_array_of_unsigned_16_1;

  \register'\ <= ds3.tup3_sel2_unsigned;

  -- index begin
  indexvec : block
    signal vec_index : integer range 0 to 32-1;
  begin
    vec_index <= to_integer((wild3))
    -- pragma translate_off
                 mod 32
    -- pragma translate_on
                 ;
    result <= \programMem\(vec_index);
  end block;
  -- index end

  ds3 <= \c$ds2_case_scrut\.tup2_sel0_tup3;

  \c$ds2_case_scrut_fun_arg\ <= ( tup3_sel0_array_of_unsigned_16_0 => stk
                                , tup3_sel1_array_of_unsigned_16_1 => heap
                                , tup3_sel2_unsigned => \register\ );

  execute_cds2_case_scrut : entity execute
    port map
      ( \c$case_alt\ => \c$ds2_case_scrut\
      , ds           => \c$ds2_case_scrut_fun_arg\
      , ds1          => result );

  wild3 <= (signed(std_logic_vector(resize(pc,64))));

  stk <= ds.tup5_sel2_array_of_unsigned_16_0;

  heap <= ds.tup5_sel3_array_of_unsigned_16_1;

  \register\ <= ds.tup5_sel4_unsigned_1;

  instr <= x.tup2_2_sel1_instr;


end;

