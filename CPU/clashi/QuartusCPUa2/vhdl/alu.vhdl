-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.cpu_fixed_types.all;

entity alu is
  port(ds     : in cpu_fixed_types.opc;
       x      : in unsigned(15 downto 0);
       y      : in unsigned(15 downto 0);
       result : out unsigned(15 downto 0));
end;

architecture structural of alu is


begin
  with (ds) select
    result <= x + y when "0",
              resize(x * y, 16) when others;


end;

