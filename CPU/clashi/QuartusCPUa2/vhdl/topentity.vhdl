-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.cpu_fixed_types.all;

entity topentity is
  port(-- clock
       clk   : in cpu_fixed_types.clk_system;
       -- reset
       rst   : in cpu_fixed_types.rst_system;
       x     : in boolean;
       \out\ : out cpu_fixed_types.maybe);
end;

architecture structural of topentity is
  signal \c$tup_app_arg\            : unsigned(4 downto 0);
  -- CPU_Fixed.hs:115:1-5
  signal pc                         : unsigned(4 downto 0);
  signal \c$tup_case_alt\           : unsigned(4 downto 0);
  -- CPU_Fixed.hs:115:1-5
  signal ds                         : cpu_fixed_types.tup4;
  -- CPU_Fixed.hs:115:1-5
  signal \stk'\                     : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:115:1-5
  signal \heap'\                    : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:115:1-5
  signal \register'\                : unsigned(15 downto 0);
  -- CPU_Fixed.hs:115:1-5
  signal ds2                        : cpu_fixed_types.tup3;
  -- CPU_Fixed.hs:115:1-5
  signal \c$ds1_case_scrut\         : cpu_fixed_types.tup2;
  -- CPU_Fixed.hs:115:1-5
  signal \c$ds1_app_arg\            : cpu_fixed_types.instr;
  -- CPU_Fixed.hs:115:1-5
  signal stk                        : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:115:1-5
  signal heap                       : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:115:1-5
  signal \register\                 : unsigned(15 downto 0);
  -- CPU_Fixed.hs:115:1-5
  signal wild3                      : signed(63 downto 0);
  signal \c$ds1_case_scrut_fun_arg\ : cpu_fixed_types.tup3;
  signal \c$vec\                    : cpu_fixed_types.array_of_instr(0 to 23);

begin
  with (pc) select
    \c$tup_app_arg\ <= to_unsigned(0,5) when "10111",
                       \c$tup_case_alt\ when others;

  pc <= ds.tup4_sel0_unsigned_0;

  \c$tup_case_alt\ <= pc + to_unsigned(1,5) when x else
                      pc;

  -- register begin
  topentity_register : block
    signal ds_reg : cpu_fixed_types.tup4 := ( tup4_sel0_unsigned_0 => to_unsigned(0,5), tup4_sel1_array_of_unsigned_16_0 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup4_sel2_array_of_unsigned_16_1 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(10,16), to_unsigned(11,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup4_sel3_unsigned_1 => to_unsigned(0,16) );
  begin
    ds <= ds_reg; 
    ds_r : process(clk,rst)
    begin
      if rst =  '1'  then
        ds_reg <= ( tup4_sel0_unsigned_0 => to_unsigned(0,5), tup4_sel1_array_of_unsigned_16_0 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup4_sel2_array_of_unsigned_16_1 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(10,16), to_unsigned(11,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup4_sel3_unsigned_1 => to_unsigned(0,16) )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      elsif rising_edge(clk) then
        ds_reg <= ( tup4_sel0_unsigned_0 => \c$tup_app_arg\, tup4_sel1_array_of_unsigned_16_0 => \stk'\, tup4_sel2_array_of_unsigned_16_1 => \heap'\, tup4_sel3_unsigned_1 => \register'\ )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      end if;
    end process;
  end block;
  -- register end

  \out\ <= \c$ds1_case_scrut\.tup2_sel1_maybe;

  \stk'\ <= ds2.tup3_sel0_array_of_unsigned_16_0;

  \heap'\ <= ds2.tup3_sel1_array_of_unsigned_16_1;

  \register'\ <= ds2.tup3_sel2_unsigned;

  ds2 <= \c$ds1_case_scrut\.tup2_sel0_tup3;

  \c$ds1_case_scrut_fun_arg\ <= ( tup3_sel0_array_of_unsigned_16_0 => stk
                                , tup3_sel1_array_of_unsigned_16_1 => heap
                                , tup3_sel2_unsigned => \register\ );

  execute_cds1_case_scrut : entity execute
    port map
      ( \c$case_alt\ => \c$ds1_case_scrut\
      , ds           => \c$ds1_case_scrut_fun_arg\
      , ds1          => \c$ds1_app_arg\ );

  \c$vec\ <= cpu_fixed_types.array_of_instr'( std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(2,16))))))
                                            , std_logic_vector'("000" & (std_logic_vector'("01" & (std_logic_vector(to_unsigned(0,16))))))
                                            , std_logic_vector'("011" & "------------------")
                                            , std_logic_vector'("001" & ("1") & "-----------------")
                                            , std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(3,16))))))
                                            , std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(4,16))))))
                                            , std_logic_vector'("000" & (std_logic_vector'("01" & (std_logic_vector(to_unsigned(1,16))))))
                                            , std_logic_vector'("011" & "------------------")
                                            , std_logic_vector'("001" & ("0") & "-----------------")
                                            , std_logic_vector'("011" & "------------------")
                                            , std_logic_vector'("001" & ("1") & "-----------------")
                                            , std_logic_vector'("011" & "------------------")
                                            , std_logic_vector'("001" & ("0") & "-----------------")
                                            , std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(12,16))))))
                                            , std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(5,16))))))
                                            , std_logic_vector'("011" & "------------------")
                                            , std_logic_vector'("001" & ("0") & "-----------------")
                                            , std_logic_vector'("011" & "------------------")
                                            , std_logic_vector'("001" & ("1") & "-----------------")
                                            , std_logic_vector'("010" & (std_logic_vector'("10" & "----------------")))
                                            , std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(2,16))))))
                                            , std_logic_vector'("010" & (std_logic_vector'("10" & "----------------")))
                                            , std_logic_vector'("011" & "------------------")
                                            , std_logic_vector'("010" & (std_logic_vector'("10" & "----------------"))) );

  -- index begin
  indexvec : block
    signal vec_index : integer range 0 to 24-1;
  begin
    vec_index <= to_integer((wild3))
    -- pragma translate_off
                 mod 24
    -- pragma translate_on
                 ;
    \c$ds1_app_arg\ <= \c$vec\(vec_index);
  end block;
  -- index end

  stk <= ds.tup4_sel1_array_of_unsigned_16_0;

  heap <= ds.tup4_sel2_array_of_unsigned_16_1;

  \register\ <= ds.tup4_sel3_unsigned_1;

  wild3 <= (signed(std_logic_vector(resize(pc,64))));


end;

