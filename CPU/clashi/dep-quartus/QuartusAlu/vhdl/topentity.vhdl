-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.cpu_hpstpcreg_types.all;

entity topentity is
  port(\c$arg\   : in cpu_hpstpcreg_types.opc;
       \c$arg_0\ : in unsigned(15 downto 0);
       \c$arg_1\ : in unsigned(15 downto 0);
       result    : out unsigned(15 downto 0));
end;

architecture structural of topentity is


begin
  with (\c$arg\) select
    result <= \c$arg_0\ + \c$arg_1\ when "0",
              resize(\c$arg_0\ * \c$arg_1\, 16) when others;


end;

