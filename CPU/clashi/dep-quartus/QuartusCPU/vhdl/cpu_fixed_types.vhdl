library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package cpu_fixed_types is

  subtype maybe is std_logic_vector(16 downto 0);
  subtype opc is std_logic_vector(0 downto 0);
  type array_of_unsigned_16 is array (integer range <>) of unsigned(15 downto 0);
  type tup3 is record
    tup3_sel0_array_of_unsigned_16_0 : cpu_fixed_types.array_of_unsigned_16(0 to 31);
    tup3_sel1_array_of_unsigned_16_1 : cpu_fixed_types.array_of_unsigned_16(0 to 31);
    tup3_sel2_unsigned : unsigned(15 downto 0);
  end record;
  type tup2 is record
    tup2_sel0_tup3 : cpu_fixed_types.tup3;
    tup2_sel1_maybe : cpu_fixed_types.maybe;
  end record;
  type tup4 is record
    tup4_sel0_unsigned_0 : unsigned(15 downto 0);
    tup4_sel1_array_of_unsigned_16_0 : cpu_fixed_types.array_of_unsigned_16(0 to 31);
    tup4_sel2_array_of_unsigned_16_1 : cpu_fixed_types.array_of_unsigned_16(0 to 31);
    tup4_sel3_unsigned_1 : unsigned(15 downto 0);
  end record;
  type tup2_0 is record
    tup2_0_sel0_array_of_unsigned_16_0 : cpu_fixed_types.array_of_unsigned_16(0 to 0);
    tup2_0_sel1_array_of_unsigned_16_1 : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  end record;
  subtype rst_system is std_logic;
  type tup2_1 is record
    tup2_1_sel0_array_of_unsigned_16_0 : cpu_fixed_types.array_of_unsigned_16(0 to 31);
    tup2_1_sel1_array_of_unsigned_16_1 : cpu_fixed_types.array_of_unsigned_16(0 to 0);
  end record;

  subtype clk_system is std_logic;
  subtype value is std_logic_vector(17 downto 0);
  subtype instr is std_logic_vector(19 downto 0);
  type array_of_instr is array (integer range <>) of cpu_fixed_types.instr;
  function toSLV (u : in unsigned) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return unsigned;
  function toSLV (slv : in std_logic_vector) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return std_logic_vector;
  function toSLV (value :  cpu_fixed_types.array_of_unsigned_16) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.array_of_unsigned_16;
  function toSLV (p : cpu_fixed_types.tup3) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.tup3;
  function toSLV (p : cpu_fixed_types.tup2) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.tup2;
  function toSLV (p : cpu_fixed_types.tup4) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.tup4;
  function toSLV (p : cpu_fixed_types.tup2_0) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.tup2_0;
  function toSLV (sl : in std_logic) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return std_logic;
  function toSLV (p : cpu_fixed_types.tup2_1) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.tup2_1;
  function toSLV (s : in signed) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return signed;
  function toSLV (value :  cpu_fixed_types.array_of_instr) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.array_of_instr;
end;

package body cpu_fixed_types is
  function toSLV (u : in unsigned) return std_logic_vector is
  begin
    return std_logic_vector(u);
  end;
  function fromSLV (slv : in std_logic_vector) return unsigned is
  begin
    return unsigned(slv);
  end;
  function toSLV (slv : in std_logic_vector) return std_logic_vector is
  begin
    return slv;
  end;
  function fromSLV (slv : in std_logic_vector) return std_logic_vector is
  begin
    return slv;
  end;
  function toSLV (value :  cpu_fixed_types.array_of_unsigned_16) return std_logic_vector is
    alias ivalue    : cpu_fixed_types.array_of_unsigned_16(1 to value'length) is value;
    variable result : std_logic_vector(1 to value'length * 16);
  begin
    for i in ivalue'range loop
      result(((i - 1) * 16) + 1 to i*16) := toSLV(ivalue(i));
    end loop;
    return result;
  end;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.array_of_unsigned_16 is
    alias islv      : std_logic_vector(0 to slv'length - 1) is slv;
    variable result : cpu_fixed_types.array_of_unsigned_16(0 to slv'length / 16 - 1);
  begin
    for i in result'range loop
      result(i) := fromSLV(islv(i * 16 to (i+1) * 16 - 1));
    end loop;
    return result;
  end;
  function toSLV (p : cpu_fixed_types.tup3) return std_logic_vector is
  begin
    return (toSLV(p.tup3_sel0_array_of_unsigned_16_0) & toSLV(p.tup3_sel1_array_of_unsigned_16_1) & toSLV(p.tup3_sel2_unsigned));
  end;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.tup3 is
  alias islv : std_logic_vector(0 to slv'length - 1) is slv;
  begin
    return (fromSLV(islv(0 to 511)),fromSLV(islv(512 to 1023)),fromSLV(islv(1024 to 1039)));
  end;
  function toSLV (p : cpu_fixed_types.tup2) return std_logic_vector is
  begin
    return (toSLV(p.tup2_sel0_tup3) & toSLV(p.tup2_sel1_maybe));
  end;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.tup2 is
  alias islv : std_logic_vector(0 to slv'length - 1) is slv;
  begin
    return (fromSLV(islv(0 to 1039)),fromSLV(islv(1040 to 1056)));
  end;
  function toSLV (p : cpu_fixed_types.tup4) return std_logic_vector is
  begin
    return (toSLV(p.tup4_sel0_unsigned_0) & toSLV(p.tup4_sel1_array_of_unsigned_16_0) & toSLV(p.tup4_sel2_array_of_unsigned_16_1) & toSLV(p.tup4_sel3_unsigned_1));
  end;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.tup4 is
  alias islv : std_logic_vector(0 to slv'length - 1) is slv;
  begin
    return (fromSLV(islv(0 to 15)),fromSLV(islv(16 to 527)),fromSLV(islv(528 to 1039)),fromSLV(islv(1040 to 1055)));
  end;
  function toSLV (p : cpu_fixed_types.tup2_0) return std_logic_vector is
  begin
    return (toSLV(p.tup2_0_sel0_array_of_unsigned_16_0) & toSLV(p.tup2_0_sel1_array_of_unsigned_16_1));
  end;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.tup2_0 is
  alias islv : std_logic_vector(0 to slv'length - 1) is slv;
  begin
    return (fromSLV(islv(0 to 15)),fromSLV(islv(16 to 527)));
  end;
  function toSLV (sl : in std_logic) return std_logic_vector is
  begin
    return std_logic_vector'(0 => sl);
  end;
  function fromSLV (slv : in std_logic_vector) return std_logic is
    alias islv : std_logic_vector (0 to slv'length - 1) is slv;
  begin
    return islv(0);
  end;
  function toSLV (p : cpu_fixed_types.tup2_1) return std_logic_vector is
  begin
    return (toSLV(p.tup2_1_sel0_array_of_unsigned_16_0) & toSLV(p.tup2_1_sel1_array_of_unsigned_16_1));
  end;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.tup2_1 is
  alias islv : std_logic_vector(0 to slv'length - 1) is slv;
  begin
    return (fromSLV(islv(0 to 511)),fromSLV(islv(512 to 527)));
  end;
  function toSLV (s : in signed) return std_logic_vector is
  begin
    return std_logic_vector(s);
  end;
  function fromSLV (slv : in std_logic_vector) return signed is
  begin
    return signed(slv);
  end;
  function toSLV (value :  cpu_fixed_types.array_of_instr) return std_logic_vector is
    alias ivalue    : cpu_fixed_types.array_of_instr(1 to value'length) is value;
    variable result : std_logic_vector(1 to value'length * 20);
  begin
    for i in ivalue'range loop
      result(((i - 1) * 20) + 1 to i*20) := toSLV(ivalue(i));
    end loop;
    return result;
  end;
  function fromSLV (slv : in std_logic_vector) return cpu_fixed_types.array_of_instr is
    alias islv      : std_logic_vector(0 to slv'length - 1) is slv;
    variable result : cpu_fixed_types.array_of_instr(0 to slv'length / 20 - 1);
  begin
    for i in result'range loop
      result(i) := fromSLV(islv(i * 20 to (i+1) * 20 - 1));
    end loop;
    return result;
  end;
end;

