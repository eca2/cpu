-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.cpu_fixed_types.all;

entity topentity is
  port(-- clock
       clk   : in cpu_fixed_types.clk_system;
       -- reset
       rst   : in cpu_fixed_types.rst_system;
       x_0   : in cpu_fixed_types.instr;
       x_1   : in cpu_fixed_types.instr;
       x_2   : in cpu_fixed_types.instr;
       x_3   : in cpu_fixed_types.instr;
       x_4   : in cpu_fixed_types.instr;
       x_5   : in cpu_fixed_types.instr;
       x_6   : in cpu_fixed_types.instr;
       x_7   : in cpu_fixed_types.instr;
       x_8   : in cpu_fixed_types.instr;
       x_9   : in cpu_fixed_types.instr;
       x_10  : in cpu_fixed_types.instr;
       x_11  : in cpu_fixed_types.instr;
       x_12  : in cpu_fixed_types.instr;
       x_13  : in cpu_fixed_types.instr;
       x_14  : in cpu_fixed_types.instr;
       x_15  : in cpu_fixed_types.instr;
       x_16  : in cpu_fixed_types.instr;
       x_17  : in cpu_fixed_types.instr;
       x_18  : in cpu_fixed_types.instr;
       x_19  : in cpu_fixed_types.instr;
       x_20  : in cpu_fixed_types.instr;
       x_21  : in cpu_fixed_types.instr;
       x_22  : in cpu_fixed_types.instr;
       x_23  : in cpu_fixed_types.instr;
       \out\ : out cpu_fixed_types.maybe);
end;

architecture structural of topentity is
  -- CPU_Fixed.hs:77:1-4
  signal pc                       : unsigned(15 downto 0);
  -- CPU_Fixed.hs:77:1-4
  signal ds                       : cpu_fixed_types.tup4;
  -- CPU_Fixed.hs:77:1-4
  signal \stk'\                   : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:77:1-4
  signal \heap'\                  : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:77:1-4
  signal \register'\              : unsigned(15 downto 0);
  -- CPU_Fixed.hs:77:1-4
  signal ds2                      : cpu_fixed_types.tup3;
  signal \c$case_alt\             : cpu_fixed_types.tup2;
  signal \c$case_alt_0\           : cpu_fixed_types.tup2;
  signal \c$case_alt_1\           : cpu_fixed_types.tup2;
  signal \c$case_alt_2\           : cpu_fixed_types.tup2;
  -- CPU_Fixed.hs:46:1-7
  signal ds2_0                    : cpu_fixed_types.value;
  -- CPU_Fixed.hs:46:1-7
  signal ds2_1                    : cpu_fixed_types.opc;
  -- CPU_Fixed.hs:46:1-7
  signal ds2_2                    : cpu_fixed_types.value;
  signal \c$app_arg\              : unsigned(15 downto 0);
  -- CPU_Fixed.hs:46:1-7
  signal x_24                     : unsigned(15 downto 0);
  signal \c$app_arg_0\            : unsigned(15 downto 0);
  signal result                   : unsigned(15 downto 0);
  -- CPU_Fixed.hs:46:1-7
  signal wild3                    : signed(63 downto 0);
  -- CPU_Fixed.hs:46:1-7
  signal x_25                     : unsigned(15 downto 0);
  -- CPU_Fixed.hs:46:1-7
  signal x_26                     : unsigned(15 downto 0);
  signal result_0                 : unsigned(15 downto 0);
  -- CPU_Fixed.hs:46:1-7
  signal wild3_0                  : signed(63 downto 0);
  -- CPU_Fixed.hs:46:1-7
  signal x_27                     : unsigned(15 downto 0);
  -- CPU_Fixed.hs:77:1-4
  signal \c$ds1_app_arg\          : cpu_fixed_types.instr;
  -- CPU_Fixed.hs:77:1-4
  signal stk                      : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:77:1-4
  signal heap                     : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:77:1-4
  signal \register\               : unsigned(15 downto 0);
  -- CPU_Fixed.hs:77:1-4
  signal wild3_1                  : signed(63 downto 0);
  signal x                        : cpu_fixed_types.array_of_instr(0 to 23);
  signal \c$vec\                  : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_1_sel_alt\   : cpu_fixed_types.tup2_0;
  signal \c$vec_0\                : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_1_sel_alt_0\ : cpu_fixed_types.tup2_1;
  signal \c$vec_1\                : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_1_sel_alt_1\ : cpu_fixed_types.tup2_0;
  signal \c$vec_2\                : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_1_sel_alt_2\ : cpu_fixed_types.tup2_1;
  signal \c$vec_3\                : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_2_sel_alt\   : cpu_fixed_types.tup2_1;
  signal \c$vec_4\                : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_2_sel_alt_0\ : cpu_fixed_types.tup2_1;

begin
  x <= cpu_fixed_types.array_of_instr'( x_0
                                      , x_1
                                      , x_2
                                      , x_3
                                      , x_4
                                      , x_5
                                      , x_6
                                      , x_7
                                      , x_8
                                      , x_9
                                      , x_10
                                      , x_11
                                      , x_12
                                      , x_13
                                      , x_14
                                      , x_15
                                      , x_16
                                      , x_17
                                      , x_18
                                      , x_19
                                      , x_20
                                      , x_21
                                      , x_22
                                      , x_23 );

  pc <= ds.tup4_sel0_unsigned_0;

  -- register begin
  topentity_register : block
    signal ds_reg : cpu_fixed_types.tup4 := ( tup4_sel0_unsigned_0 => to_unsigned(0,16), tup4_sel1_array_of_unsigned_16_0 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup4_sel2_array_of_unsigned_16_1 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(10,16), to_unsigned(11,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup4_sel3_unsigned_1 => to_unsigned(0,16) );
  begin
    ds <= ds_reg; 
    ds_r : process(clk,rst)
    begin
      if rst =  '1'  then
        ds_reg <= ( tup4_sel0_unsigned_0 => to_unsigned(0,16), tup4_sel1_array_of_unsigned_16_0 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup4_sel2_array_of_unsigned_16_1 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(10,16), to_unsigned(11,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup4_sel3_unsigned_1 => to_unsigned(0,16) )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      elsif rising_edge(clk) then
        ds_reg <= ( tup4_sel0_unsigned_0 => to_unsigned(1,16) + pc, tup4_sel1_array_of_unsigned_16_0 => \stk'\, tup4_sel2_array_of_unsigned_16_1 => \heap'\, tup4_sel3_unsigned_1 => \register'\ )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      end if;
    end process;
  end block;
  -- register end

  \out\ <= \c$case_alt\.tup2_sel1_maybe;

  \stk'\ <= ds2.tup3_sel0_array_of_unsigned_16_0;

  \heap'\ <= ds2.tup3_sel1_array_of_unsigned_16_1;

  \register'\ <= ds2.tup3_sel2_unsigned;

  ds2 <= \c$case_alt\.tup2_sel0_tup3;

  with (\c$ds1_app_arg\(19 downto 18)) select
    \c$case_alt\ <= \c$case_alt_2\ when "00",
                    \c$case_alt_1\ when "01",
                    \c$case_alt_0\ when "10",
                    ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'((stk(1 to stk'high))) & cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => to_unsigned(0,16))))
                    , tup3_sel1_array_of_unsigned_16_1 => heap
                    , tup3_sel2_unsigned => \c$app_arg\ )
                    , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when others;

  with (ds2_0(17 downto 16)) select
    \c$case_alt_0\ <= ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => stk
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("1" & (std_logic_vector(x_24))) ) when "00",
                      ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => stk
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("1" & (std_logic_vector(result))) ) when "01",
                      ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => stk
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("1" & (std_logic_vector(\c$app_arg_0\))) ) when others;

  \c$vec\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(stk) & cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => to_unsigned(0,16)))));

  \c$case_alt_1_sel_alt\ <= (\c$vec\(0 to 1-1),\c$vec\(1 to \c$vec\'high));

  \c$vec_0\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => \c$app_arg\ + \register\)) & cpu_fixed_types.array_of_unsigned_16'(\c$case_alt_1_sel_alt\.tup2_0_sel1_array_of_unsigned_16_1)));

  \c$case_alt_1_sel_alt_0\ <= (\c$vec_0\(0 to 32-1),\c$vec_0\(32 to \c$vec_0\'high));

  \c$vec_1\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(stk) & cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => to_unsigned(0,16)))));

  \c$case_alt_1_sel_alt_1\ <= (\c$vec_1\(0 to 1-1),\c$vec_1\(1 to \c$vec_1\'high));

  \c$vec_2\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => resize(\c$app_arg\ * \register\, 16))) & cpu_fixed_types.array_of_unsigned_16'(\c$case_alt_1_sel_alt_1\.tup2_0_sel1_array_of_unsigned_16_1)));

  \c$case_alt_1_sel_alt_2\ <= (\c$vec_2\(0 to 32-1),\c$vec_2\(32 to \c$vec_2\'high));

  with (ds2_1) select
    \c$case_alt_1\ <= ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => \c$case_alt_1_sel_alt_0\.tup2_1_sel0_array_of_unsigned_16_0
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when "0",
                      ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => \c$case_alt_1_sel_alt_2\.tup2_1_sel0_array_of_unsigned_16_0
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when others;

  \c$vec_3\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => x_26)) & cpu_fixed_types.array_of_unsigned_16'(stk)));

  \c$case_alt_2_sel_alt\ <= (\c$vec_3\(0 to 32-1),\c$vec_3\(32 to \c$vec_3\'high));

  \c$vec_4\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => result_0)) & cpu_fixed_types.array_of_unsigned_16'(stk)));

  \c$case_alt_2_sel_alt_0\ <= (\c$vec_4\(0 to 32-1),\c$vec_4\(32 to \c$vec_4\'high));

  with (ds2_2(17 downto 16)) select
    \c$case_alt_2\ <= ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => \c$case_alt_2_sel_alt\.tup2_1_sel0_array_of_unsigned_16_0
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when "00",
                      ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => \c$case_alt_2_sel_alt_0\.tup2_1_sel0_array_of_unsigned_16_0
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when "01",
                      cpu_fixed_types.tup2'( cpu_fixed_types.tup3'( cpu_fixed_types.array_of_unsigned_16'(0 to 31 => unsigned'(0 to 15 => '-')), cpu_fixed_types.array_of_unsigned_16'(0 to 31 => unsigned'(0 to 15 => '-')), unsigned'(0 to 15 => '-') ), cpu_fixed_types.maybe'(0 to 16 => '-') ) when others;

  ds2_0 <= \c$ds1_app_arg\(17 downto 0);

  ds2_1 <= \c$ds1_app_arg\(17 downto 17);

  ds2_2 <= \c$ds1_app_arg\(17 downto 0);

  \c$app_arg\ <=  stk(0) ;

  x_24 <= unsigned(ds2_0(15 downto 0));

  -- index begin
  indexvec : block
    signal vec_index : integer range 0 to 32-1;
  begin
    vec_index <= to_integer(to_signed(0,64))
    -- pragma translate_off
                 mod 32
    -- pragma translate_on
                 ;
    \c$app_arg_0\ <= stk(vec_index);
  end block;
  -- index end

  -- index begin
  indexvec_0 : block
    signal vec_index_0 : integer range 0 to 32-1;
  begin
    vec_index_0 <= to_integer((wild3))
    -- pragma translate_off
                 mod 32
    -- pragma translate_on
                 ;
    result <= heap(vec_index_0);
  end block;
  -- index end

  wild3 <= (signed(std_logic_vector(resize(x_25,64))));

  x_25 <= unsigned(ds2_0(15 downto 0));

  x_26 <= unsigned(ds2_2(15 downto 0));

  -- index begin
  indexvec_1 : block
    signal vec_index_1 : integer range 0 to 32-1;
  begin
    vec_index_1 <= to_integer((wild3_0))
    -- pragma translate_off
                 mod 32
    -- pragma translate_on
                 ;
    result_0 <= heap(vec_index_1);
  end block;
  -- index end

  wild3_0 <= (signed(std_logic_vector(resize(x_27,64))));

  x_27 <= unsigned(ds2_2(15 downto 0));

  -- index begin
  indexvec_2 : block
    signal vec_index_2 : integer range 0 to 24-1;
  begin
    vec_index_2 <= to_integer((wild3_1))
    -- pragma translate_off
                 mod 24
    -- pragma translate_on
                 ;
    \c$ds1_app_arg\ <= x(vec_index_2);
  end block;
  -- index end

  stk <= ds.tup4_sel1_array_of_unsigned_16_0;

  heap <= ds.tup4_sel2_array_of_unsigned_16_1;

  \register\ <= ds.tup4_sel3_unsigned_1;

  wild3_1 <= (signed(std_logic_vector(resize(pc,64))));


end;

