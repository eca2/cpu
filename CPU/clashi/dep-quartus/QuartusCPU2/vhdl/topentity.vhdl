-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.cpu_fixed_types.all;

entity topentity is
  port(-- clock
       clk   : in cpu_fixed_types.clk_system;
       -- reset
       rst   : in cpu_fixed_types.rst_system;
       x_0   : in boolean;
       x_1   : in cpu_fixed_types.instr_0;
       \out\ : out cpu_fixed_types.maybe);
end;

architecture structural of topentity is
  signal \c$tup_app_arg\           : unsigned(4 downto 0);
  signal \c$tup_app_arg_0\         : cpu_fixed_types.array_of_instr_0(0 to 31);
  -- CPU_Fixed.hs:77:1-10
  signal \programMem\              : cpu_fixed_types.array_of_instr_0(0 to 31);
  -- CPU_Fixed.hs:77:1-10
  signal pc                        : unsigned(4 downto 0);
  -- CPU_Fixed.hs:77:1-10
  signal is_write                  : boolean;
  signal \c$tup_case_alt\          : unsigned(4 downto 0);
  -- CPU_Fixed.hs:77:1-10
  signal ds                        : cpu_fixed_types.tup5;
  signal \c$tup_case_alt_0\        : unsigned(4 downto 0);
  -- CPU_Fixed.hs:77:1-10
  signal \stk'\                    : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:77:1-10
  signal \heap'\                   : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:77:1-10
  signal \register'\               : unsigned(15 downto 0);
  signal result                    : cpu_fixed_types.instr_0;
  -- CPU_Fixed.hs:77:1-10
  signal ds3                       : cpu_fixed_types.tup3;
  signal \c$case_alt\              : cpu_fixed_types.tup2;
  signal \c$case_alt_0\            : cpu_fixed_types.tup2;
  signal \c$case_alt_1\            : cpu_fixed_types.tup2;
  signal \c$case_alt_2\            : cpu_fixed_types.tup2;
  -- CPU_Fixed.hs:45:1-7
  signal ds2                       : cpu_fixed_types.value;
  -- CPU_Fixed.hs:45:1-7
  signal ds2_0                     : cpu_fixed_types.opc;
  -- CPU_Fixed.hs:45:1-7
  signal ds2_1                     : cpu_fixed_types.value;
  signal \c$app_arg\               : unsigned(15 downto 0);
  -- CPU_Fixed.hs:45:1-7
  signal x_2                       : unsigned(15 downto 0);
  signal \c$app_arg_0\             : unsigned(15 downto 0);
  signal result_0                  : unsigned(15 downto 0);
  -- CPU_Fixed.hs:45:1-7
  signal wild3                     : signed(63 downto 0);
  -- CPU_Fixed.hs:45:1-7
  signal x_3                       : unsigned(15 downto 0);
  -- CPU_Fixed.hs:45:1-7
  signal x_4                       : unsigned(15 downto 0);
  signal result_1                  : unsigned(15 downto 0);
  -- CPU_Fixed.hs:45:1-7
  signal wild3_0                   : signed(63 downto 0);
  -- CPU_Fixed.hs:45:1-7
  signal x_5                       : unsigned(15 downto 0);
  -- CPU_Fixed.hs:77:1-10
  signal wild3_1                   : signed(63 downto 0);
  -- CPU_Fixed.hs:77:1-10
  signal stk                       : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:77:1-10
  signal heap                      : cpu_fixed_types.array_of_unsigned_16(0 to 31);
  -- CPU_Fixed.hs:77:1-10
  signal \register\                : unsigned(15 downto 0);
  -- CPU_Fixed.hs:77:1-10
  signal instr                     : cpu_fixed_types.instr_0;
  signal x                         : cpu_fixed_types.tup2_0;
  signal \c$vec\                   : cpu_fixed_types.array_of_instr_0(0 to 32);
  signal \c$tup_app_arg_0_sel_alt\ : cpu_fixed_types.tup2_1;
  signal \c$vec_0\                 : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_1_sel_alt\    : cpu_fixed_types.tup2_2;
  signal \c$vec_1\                 : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_1_sel_alt_0\  : cpu_fixed_types.tup2_3;
  signal \c$vec_2\                 : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_1_sel_alt_1\  : cpu_fixed_types.tup2_2;
  signal \c$vec_3\                 : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_1_sel_alt_2\  : cpu_fixed_types.tup2_3;
  signal \c$vec_4\                 : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_2_sel_alt\    : cpu_fixed_types.tup2_3;
  signal \c$vec_5\                 : cpu_fixed_types.array_of_unsigned_16(0 to 32);
  signal \c$case_alt_2_sel_alt_0\  : cpu_fixed_types.tup2_3;

begin
  x <= ( tup2_0_sel0_boolean => x_0
       , tup2_0_sel1_instr_0 => x_1 );

  with (result(20 downto 18)) select
    \c$tup_app_arg\ <= pc when "100",
                       \c$tup_case_alt\ when others;

  \c$vec\ <= (cpu_fixed_types.array_of_instr_0'(cpu_fixed_types.array_of_instr_0'(cpu_fixed_types.array_of_instr_0'(0 => instr)) & cpu_fixed_types.array_of_instr_0'(\programMem\)));

  \c$tup_app_arg_0_sel_alt\ <= (\c$vec\(0 to 32-1),\c$vec\(32 to \c$vec\'high));

  \c$tup_app_arg_0\ <= \c$tup_app_arg_0_sel_alt\.tup2_1_sel0_array_of_instr_0_0 when is_write else
                       \programMem\;

  \programMem\ <= ds.tup5_sel0_array_of_instr_0;

  pc <= ds.tup5_sel1_unsigned_0;

  is_write <= x.tup2_0_sel0_boolean;

  \c$tup_case_alt\ <= pc when is_write else
                      \c$tup_case_alt_0\;

  -- register begin
  topentity_register : block
    signal ds_reg : cpu_fixed_types.tup5 := ( tup5_sel0_array_of_instr_0 => cpu_fixed_types.array_of_instr_0'( std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(2,16)))))), std_logic_vector'("000" & (std_logic_vector'("01" & (std_logic_vector(to_unsigned(0,16)))))), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("1") & "-----------------"), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(3,16)))))), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(4,16)))))), std_logic_vector'("000" & (std_logic_vector'("01" & (std_logic_vector(to_unsigned(1,16)))))), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("0") & "-----------------"), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("1") & "-----------------"), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("0") & "-----------------"), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(12,16)))))), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(5,16)))))), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("0") & "-----------------"), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("1") & "-----------------"), std_logic_vector'("010" & (std_logic_vector'("10" & "----------------"))), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(2,16)))))), std_logic_vector'("010" & (std_logic_vector'("10" & "----------------"))), std_logic_vector'("011" & "------------------"), std_logic_vector'("010" & (std_logic_vector'("10" & "----------------"))), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("101" & "------------------") ), tup5_sel1_unsigned_0 => to_unsigned(0,5), tup5_sel2_array_of_unsigned_16_0 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup5_sel3_array_of_unsigned_16_1 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(10,16), to_unsigned(11,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup5_sel4_unsigned_1 => to_unsigned(0,16) );
  begin
    ds <= ds_reg; 
    ds_r : process(clk,rst)
    begin
      if rst =  '1'  then
        ds_reg <= ( tup5_sel0_array_of_instr_0 => cpu_fixed_types.array_of_instr_0'( std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(2,16)))))), std_logic_vector'("000" & (std_logic_vector'("01" & (std_logic_vector(to_unsigned(0,16)))))), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("1") & "-----------------"), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(3,16)))))), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(4,16)))))), std_logic_vector'("000" & (std_logic_vector'("01" & (std_logic_vector(to_unsigned(1,16)))))), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("0") & "-----------------"), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("1") & "-----------------"), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("0") & "-----------------"), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(12,16)))))), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(5,16)))))), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("0") & "-----------------"), std_logic_vector'("011" & "------------------"), std_logic_vector'("001" & ("1") & "-----------------"), std_logic_vector'("010" & (std_logic_vector'("10" & "----------------"))), std_logic_vector'("000" & (std_logic_vector'("00" & (std_logic_vector(to_unsigned(2,16)))))), std_logic_vector'("010" & (std_logic_vector'("10" & "----------------"))), std_logic_vector'("011" & "------------------"), std_logic_vector'("010" & (std_logic_vector'("10" & "----------------"))), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("100" & "------------------"), std_logic_vector'("101" & "------------------") ), tup5_sel1_unsigned_0 => to_unsigned(0,5), tup5_sel2_array_of_unsigned_16_0 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup5_sel3_array_of_unsigned_16_1 => cpu_fixed_types.array_of_unsigned_16'( to_unsigned(10,16), to_unsigned(11,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16), to_unsigned(0,16) ), tup5_sel4_unsigned_1 => to_unsigned(0,16) )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      elsif rising_edge(clk) then
        ds_reg <= ( tup5_sel0_array_of_instr_0 => \c$tup_app_arg_0\, tup5_sel1_unsigned_0 => \c$tup_app_arg\, tup5_sel2_array_of_unsigned_16_0 => \stk'\, tup5_sel3_array_of_unsigned_16_1 => \heap'\, tup5_sel4_unsigned_1 => \register'\ )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      end if;
    end process;
  end block;
  -- register end

  with (result(20 downto 18)) select
    \c$tup_case_alt_0\ <= to_unsigned(0,5) when "101",
                          pc + to_unsigned(1,5) when others;

  \out\ <= \c$case_alt\.tup2_sel1_maybe;

  \stk'\ <= ds3.tup3_sel0_array_of_unsigned_16_0;

  \heap'\ <= ds3.tup3_sel1_array_of_unsigned_16_1;

  \register'\ <= ds3.tup3_sel2_unsigned;

  -- index begin
  indexvec : block
    signal vec_index : integer range 0 to 32-1;
  begin
    vec_index <= to_integer((wild3_1))
    -- pragma translate_off
                 mod 32
    -- pragma translate_on
                 ;
    result <= \programMem\(vec_index);
  end block;
  -- index end

  ds3 <= \c$case_alt\.tup2_sel0_tup3;

  with (result(20 downto 18)) select
    \c$case_alt\ <= \c$case_alt_2\ when "000",
                    \c$case_alt_1\ when "001",
                    \c$case_alt_0\ when "010",
                    ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'((stk(1 to stk'high))) & cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => to_unsigned(0,16))))
                    , tup3_sel1_array_of_unsigned_16_1 => heap
                    , tup3_sel2_unsigned => \c$app_arg\ )
                    , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when "011",
                    ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => stk
                    , tup3_sel1_array_of_unsigned_16_1 => heap
                    , tup3_sel2_unsigned => \register\ )
                    , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when "100",
                    cpu_fixed_types.tup2'( cpu_fixed_types.tup3'( cpu_fixed_types.array_of_unsigned_16'(0 to 31 => unsigned'(0 to 15 => '-')), cpu_fixed_types.array_of_unsigned_16'(0 to 31 => unsigned'(0 to 15 => '-')), unsigned'(0 to 15 => '-') ), cpu_fixed_types.maybe'(0 to 16 => '-') ) when others;

  with (ds2(17 downto 16)) select
    \c$case_alt_0\ <= ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => stk
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("1" & (std_logic_vector(x_2))) ) when "00",
                      ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => stk
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("1" & (std_logic_vector(result_0))) ) when "01",
                      ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => stk
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("1" & (std_logic_vector(\c$app_arg_0\))) ) when others;

  \c$vec_0\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(stk) & cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => to_unsigned(0,16)))));

  \c$case_alt_1_sel_alt\ <= (\c$vec_0\(0 to 1-1),\c$vec_0\(1 to \c$vec_0\'high));

  \c$vec_1\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => \c$app_arg\ + \register\)) & cpu_fixed_types.array_of_unsigned_16'(\c$case_alt_1_sel_alt\.tup2_2_sel1_array_of_unsigned_16_1)));

  \c$case_alt_1_sel_alt_0\ <= (\c$vec_1\(0 to 32-1),\c$vec_1\(32 to \c$vec_1\'high));

  \c$vec_2\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(stk) & cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => to_unsigned(0,16)))));

  \c$case_alt_1_sel_alt_1\ <= (\c$vec_2\(0 to 1-1),\c$vec_2\(1 to \c$vec_2\'high));

  \c$vec_3\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => resize(\c$app_arg\ * \register\, 16))) & cpu_fixed_types.array_of_unsigned_16'(\c$case_alt_1_sel_alt_1\.tup2_2_sel1_array_of_unsigned_16_1)));

  \c$case_alt_1_sel_alt_2\ <= (\c$vec_3\(0 to 32-1),\c$vec_3\(32 to \c$vec_3\'high));

  with (ds2_0) select
    \c$case_alt_1\ <= ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => \c$case_alt_1_sel_alt_0\.tup2_3_sel0_array_of_unsigned_16_0
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when "0",
                      ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => \c$case_alt_1_sel_alt_2\.tup2_3_sel0_array_of_unsigned_16_0
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when others;

  \c$vec_4\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => x_4)) & cpu_fixed_types.array_of_unsigned_16'(stk)));

  \c$case_alt_2_sel_alt\ <= (\c$vec_4\(0 to 32-1),\c$vec_4\(32 to \c$vec_4\'high));

  \c$vec_5\ <= (cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(cpu_fixed_types.array_of_unsigned_16'(0 => result_1)) & cpu_fixed_types.array_of_unsigned_16'(stk)));

  \c$case_alt_2_sel_alt_0\ <= (\c$vec_5\(0 to 32-1),\c$vec_5\(32 to \c$vec_5\'high));

  with (ds2_1(17 downto 16)) select
    \c$case_alt_2\ <= ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => \c$case_alt_2_sel_alt\.tup2_3_sel0_array_of_unsigned_16_0
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when "00",
                      ( tup2_sel0_tup3 => ( tup3_sel0_array_of_unsigned_16_0 => \c$case_alt_2_sel_alt_0\.tup2_3_sel0_array_of_unsigned_16_0
                      , tup3_sel1_array_of_unsigned_16_1 => heap
                      , tup3_sel2_unsigned => \register\ )
                      , tup2_sel1_maybe => std_logic_vector'("0" & "----------------") ) when "01",
                      cpu_fixed_types.tup2'( cpu_fixed_types.tup3'( cpu_fixed_types.array_of_unsigned_16'(0 to 31 => unsigned'(0 to 15 => '-')), cpu_fixed_types.array_of_unsigned_16'(0 to 31 => unsigned'(0 to 15 => '-')), unsigned'(0 to 15 => '-') ), cpu_fixed_types.maybe'(0 to 16 => '-') ) when others;

  ds2 <= result(17 downto 0);

  ds2_0 <= result(17 downto 17);

  ds2_1 <= result(17 downto 0);

  \c$app_arg\ <=  stk(0) ;

  x_2 <= unsigned(ds2(15 downto 0));

  -- index begin
  indexvec_0 : block
    signal vec_index_0 : integer range 0 to 32-1;
  begin
    vec_index_0 <= to_integer(to_signed(0,64))
    -- pragma translate_off
                 mod 32
    -- pragma translate_on
                 ;
    \c$app_arg_0\ <= stk(vec_index_0);
  end block;
  -- index end

  -- index begin
  indexvec_1 : block
    signal vec_index_1 : integer range 0 to 32-1;
  begin
    vec_index_1 <= to_integer((wild3))
    -- pragma translate_off
                 mod 32
    -- pragma translate_on
                 ;
    result_0 <= heap(vec_index_1);
  end block;
  -- index end

  wild3 <= (signed(std_logic_vector(resize(x_3,64))));

  x_3 <= unsigned(ds2(15 downto 0));

  x_4 <= unsigned(ds2_1(15 downto 0));

  -- index begin
  indexvec_2 : block
    signal vec_index_2 : integer range 0 to 32-1;
  begin
    vec_index_2 <= to_integer((wild3_0))
    -- pragma translate_off
                 mod 32
    -- pragma translate_on
                 ;
    result_1 <= heap(vec_index_2);
  end block;
  -- index end

  wild3_0 <= (signed(std_logic_vector(resize(x_5,64))));

  x_5 <= unsigned(ds2_1(15 downto 0));

  wild3_1 <= (signed(std_logic_vector(resize(pc,64))));

  stk <= ds.tup5_sel2_array_of_unsigned_16_0;

  heap <= ds.tup5_sel3_array_of_unsigned_16_1;

  \register\ <= ds.tup5_sel4_unsigned_1;

  instr <= x.tup2_0_sel1_instr_0;


end;

