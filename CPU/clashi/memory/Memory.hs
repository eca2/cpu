module Memory where
import Clash.Prelude

type Clk = Clock System
type Rst = Reset System
type Sig = Signal System

type Val = Unsigned 16

data Opc = Add | Mul
 deriving (Show, Generic, NFDataX, Eq)

data Value = Cst Val | Addr Val | Top
 deriving (Show, Generic, NFDataX, Eq)

data Instr = Push Value | Calc Opc | Send Value | Pop | Idle | ResetPc
 deriving (Show, Generic, NFDataX, Eq)


fifo_prog_memory_mfunc :: Vec 32 Instr -> (Bool, Unsigned 5, Instr) -> (Vec 32 Instr, Instr)
fifo_prog_memory_mfunc state (write , adr , input) = (state' , out)
   where
    out     
      |write = Idle
      |otherwise = state !! adr

    state'
      |write  = input +>> state
      |otherwise = state
      