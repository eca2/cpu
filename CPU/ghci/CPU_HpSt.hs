module CPU_HpSt where
import Data.List

-- Student information:
--  Student 1
--    lastname: Battistig
--    student number: s1716239
--  Student 2
--    lastname: Pennestri
--    student number: s2382660

data Opc = Add | Mul
 deriving Show

data Value = Const Int | Addr Int
 deriving Show

data Instr = Push Value | Calc Opc
 deriving Show

alu :: Opc -> Int -> Int -> Int
alu Add x y = x + y
alu Mul x y = x * y


value :: Value -> [Int]-> Int
value (Addr x) heap = heap !! x
value (Const x) _ = x


core :: ([Int] , [Int]) -> Instr -> ([Int] , [Int])
core ((stkFirst:stkSecond:stkTail) , heap) (Calc Add) = (stk' , heap)
     where
        operator1 = stkFirst
        operator2 = stkSecond
        stk' = [alu Add operator1 operator2] ++ stkTail


core ((stkFirst:stkSecond:stkTail) , heap) (Calc Mul) = (stk' , heap)
     where
        operator1 = stkFirst
        operator2 = stkSecond
        stk' = [alu Mul operator1 operator2] ++ stkTail


core (stk , heap) (Push (Const x)) = (stk' , heap)
     where
        stk' = [x] ++ stk


core (stk , heap) (Push (Addr x)) = (stk' , heap)
     where
        stk' = [(heap !! x)] ++ stk



program = [ Push (Const 2 ) , Push (Addr 0 ) , Calc Mul , Push (Const 3 ) , Push (Const 4 ) , Push (Addr 1 ) , Calc Add, Calc Mul , Calc Add,Push (Const 12) , Push (Const 5 ) , Calc Add, Calc Mul ]

test = scanl core ([],[10,11]) program