module CPU_HpStPc where
import Data.List

-- Student information:
--  Student 1
--    lastname: Battistig
--    student number: s1716239
--  Student 2
--    lastname: Pennestri
--    student number: s2382660

data Opc = Add | Mul
 deriving Show

data Value = Const Int | Addr Int | Top
 deriving Show

data Instr = Push Value | Calc Opc | Send Value
 deriving Show
program:: [Instr]
program = [ Push (Const 2 ) , Push (Addr 0 ) , Calc Mul , Push (Const 3 ) , Push (Const 4 ) , Push (Addr 1 ) , Calc Add, Calc Mul , Calc Add,Push (Const 12) , Push (Const 5 ) , Calc Add, Calc Mul ]

program2:: [Instr]
program2 = [ Push (Const 2 ) , Push (Addr 0 ) , Calc Mul , Push (Const 3 ) , Push (Const 4 ) , Push (Addr 1 ) , Calc Add, Calc Mul , Calc Add,Push (Const 12) , Push (Const 5 ) , Calc Add, Calc Mul, Send Top ]


-- ---------------------** ALU definition **-----------------------
alu :: Opc -> Int -> Int -> Int
alu Add x y = x + y
alu Mul x y = x * y


value :: Value -> [Int]-> Int
value (Addr x) heap = heap !! x
value (Const x) _ = x


execute :: ([Int] , [Int]) -> Instr -> (([Int] , [Int]), Int)
execute ((stkFirst:stkSecond:stkTail) , heap) (Calc Add) = ((stk' , heap),-1)
     where
        operator1 = stkFirst
        operator2 = stkSecond
        stk' = [alu Add operator1 operator2] ++ stkTail


execute ((stkFirst:stkSecond:stkTail) , heap) (Calc Mul) = ((stk' , heap),-1)
     where
        operator1 = stkFirst
        operator2 = stkSecond
        stk' = [alu Mul operator1 operator2] ++ stkTail


execute (stk , heap) (Push (Const x)) = ((stk' , heap), -1)
     where
        stk' = [x] ++ stk


execute (stk , heap) (Push (Addr x)) = ((stk' , heap), -1)
     where
        stk' = [(heap !! x)] ++ stk


execute (stk , heap) (Send (Const x)) = ((stk , heap), x)

execute (stk , heap) (Send (Addr x)) = ((stk , heap), (heap !! x))
execute (stk , heap) (Send Top) = ((stk , heap), (stk !! 0))


-- Simulation Function
sim f s [] = []
sim f s (x:xs) = z:sim f s' xs
    where
        (s',z) = f s x
--test = sim execute ([],[11,10]) program


-- execute function with PC
core :: [Instr] -> (Int , [Int] , [Int]) -> Int -> ((Int, [Int], [Int]), Int)
core prog (pc, stk, heap) tick = ((pc', stk', heap'), out)
       where
        pc' = tick + pc
        ((stk' , heap'), out) = execute (stk, heap) (prog !! pc)


test = sim (core program2) (0,[],[10, 11]) $ repeat 1