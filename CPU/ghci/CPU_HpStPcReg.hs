module CPU_HpStPcReg where
import Data.List

-- Student information:
--  Student 1
--    lastname: Battistig
--    student number: s1716239
--  Student 2
--    lastname: Pennestri
--    student number: s2382660

data Opc = Add | Mul
 deriving Show

data Value = Const Int | Addr Int | Top
 deriving Show

data Instr = Push Value | Calc Opc | Send Value | Pop
 deriving Show
program:: [Instr]
program = [ Push ( Const 2 ) , Push ( Addr 0 ) , Pop , Calc Mul , Push ( Const 3 ) , Push ( Const 4 ) , Push ( Addr 1 ) , Pop , Calc Add , Pop , Calc Mul , Pop , Calc Add , Push ( Const 12 ) , Push ( Const 5 ) , Pop , Calc Add , Pop , Calc Mul , Send Top , Push ( Const 2 ) , Send Top , Pop , Send Top ]
-- ---------------------** ALU definition **-----------------------
alu :: Opc -> Int -> Int -> Int
alu Add x y = x + y
alu Mul x y = x * y

value :: Value -> [Int]-> Int
value (Addr x) heap = heap !! x
value (Const x) _ = x

execute :: ([Int] , [Int] , Int) -> Instr -> (([Int] , [Int], Int), Int)
execute ((stkFirst:stkTail) , heap, register) (Calc Add) = ((stk' , heap, register),-1)
     where
        operator1 = stkFirst
        stk' = [alu Add operator1 register] ++ stkTail


execute ((stkFirst:stkTail) , heap, register) (Calc Mul) = ((stk' , heap, register),-1)
     where
        operator1 = stkFirst
        stk' = [alu Mul operator1 register] ++ stkTail


execute ((stkFirst:stkTail) , heap, register) (Pop) = ((stkTail , heap, stkFirst),-1)


execute (stk, heap, register) (Push (Const x)) = ((stk' , heap, register), -1)
     where
        stk' = [x] ++ stk


execute (stk, heap, register) (Push (Addr x)) = ((stk' , heap, register), -1)
     where
        stk' = [(heap !! x)] ++ stk

execute (stk , heap, register) (Send (Const x)) = ((stk , heap, register), x)
execute (stk , heap, register) (Send (Addr x)) = ((stk , heap, register), (heap !! x))
execute (stk , heap, register) (Send Top) = ((stk , heap, register), (stk !! 0))


-- Simulation Function
sim f s [] = []
sim f s (x:xs) = z:sim f s' xs
    where
        (s',z) = f s x



-- execute function with PC
core :: [Instr] -> (Int , [Int] , [Int], Int) -> Int -> ((Int, [Int], [Int], Int), Int)
core prog (pc, stk, heap, register) tick = ((pc', stk', heap', register'), out)
       where
        pc' = tick + pc
        ((stk' , heap', register'),out) = execute (stk, heap, register) (prog !! pc)

test = sim (core program) (0,[],[10, 11],0) $ repeat 1