module CPU_st where
import Data.List


data Opc = Add | Mul
 deriving Show

data Instr = Push Int | Calc Opc
 deriving Show

-- ---------------------** ALU definition **-----------------------
alu :: Opc -> Int -> Int -> Int
alu Add x y = x + y
alu Mul x y = x * y






-- ---------------------** Core Addition **-----------------------
core :: [Int] -> Instr -> [Int]
core stk (Calc Add) = newstk
 where --deletes first 2 and replaces with output of alu
 stkt = tail stk
 snd = head stkt
 frst = head stk
 stkrem = delete snd (delete frst stk)
 newstk = [alu Add frst snd] ++ stkrem



-- ---------------------** Core Multiplication **-----------------------
--core stk Mul x y = alout ++ stk
-- where -- just appends alu output to top of stack
-- alout = [alu Mul x y]

core stk (Calc Mul) = newstk
 where --deletes first 2 and replaces with output of alu
 stkt = tail stk
 snd = head stkt
 frst = head stk
 stkrem = delete snd (delete frst stk)
 newstk = [alu Mul frst snd] ++ stkrem

 
 -- ---------------------** Core push **-----------------------
core stk (Push n) = [n] ++ stk

 -- ---------------------** Simulation question 1 **-----------------------
program = [ Push 2 , Push 10 , Calc Mul , Push 3 , Push 4 , Push 11 , Calc Add , Calc Mul , Calc Add , Push 12 , Push 5 , Calc Add , Calc Mul ]
test = scanl core [] program
